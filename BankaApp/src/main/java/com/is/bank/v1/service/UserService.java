package com.is.bank.v1.service;



import java.util.List;

import com.is.bank.v1.security.model.User;

public interface UserService {

    User save(User user);
    List<User> findAll();
    void delete(long id);
}
