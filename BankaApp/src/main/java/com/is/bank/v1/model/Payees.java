package com.is.bank.v1.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Payees {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int payeeId;
	private String accountNo;
	private String ifscCode;
	@OneToOne(cascade = CascadeType.ALL)
	private Branch branch;

}
