package com.is.bank.v1.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String accountNumber;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Branch branch;
	private String accountType;
	private double accountBalance;
	@OneToMany(cascade = CascadeType.ALL)
	private List<Transaction> transaction;	

}
